<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP-KT-Activity 2</title>
</head>
<body style="padding: 10%;">
	<div style="border: 1px solid black; padding: 5%; margin: 10%;">
		<h1>Divisible By Five</h1>
			<p><?php modifiedForLoop(); ?></p>
	</div>

	<div style="border: 1px solid black; padding: 5%; margin: 10%;">
		<h1>Array Manipulation</h1>
		<p><?php array_push($students, "John Smith") ?></p>
		<p><?php var_dump($students) ?></p>
		<p><?php echo count($students) ?></p>

		<p><?php array_push($students, "Jane Smith") ?></p>
		<p><?php var_dump($students) ?></p>
		<p><?php echo count($students) ?></p>

		<p><?php array_shift($students) ?></p>
		<p><?php var_dump($students) ?></p>
		<p><?php echo count($students) ?></p>
	</div>
	
</body>
</html>